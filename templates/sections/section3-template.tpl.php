<?php
 $content = $pageVariable['content'];
 $field_sec3_type = $pageVariable['field_sec3_type'];
?>

<?php if($st === '1') : ?>
<!-- Section 3 Start -->
  <section class="col-s-4 col-l-12 sec3">
    <?php if($field_sec3_type[0]['value'] === 'five_stories') :?>
    <?php unset($content['field_fc_sec3_fs']['#prefix']);  // Unsetting the prefix Part?>
    <?php unset($content['field_fc_sec3_fs']['#suffix']); //Unsetting the suffix Part?>
      <?php print render($content['field_fc_sec3_fs']); ?>
    <?php elseif($field_sec3_type[0]['value'] === 'look_book') :?>
      <div class="sec3-inner">
        <div class="sec3-wrapper lookbook-theme">
          <?php unset($content['field_fc_sec3_lb']['#prefix']);  // Unsetting the prefix Part?>
          <?php unset($content['field_fc_sec3_lb']['#suffix']); //Unsetting the suffix Part?>
          <?php print render($content['field_fc_sec3_lb']); ?>
          <div id="sec3_slide_caption" class="slider-right-side-text">
            <div class="synopsis">
              - <?php if(array_key_exists('field_sec3_lb_eyebrow',$content)) {print render($content['field_sec3_lb_eyebrow']);}?>
            </div>
            <div class="synopsis-main-text">
              <a href="<?php array_key_exists('field_sec3_lb_link',$content) ? print $content['field_sec3_lb_link'][0]['#markup'] : print '#'; ?>">
                <?php if(array_key_exists('field_sec3_lb_headline',$content)) {print render($content['field_sec3_lb_headline']);} ?>
              </a>
              <?php if(array_key_exists('field_sec3_lb_description',$content)) {print render($content['field_sec3_lb_description']);} ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </section>
  <!-- Section 3 Ends -->
<?php endif; ?>
